ANU-Web-Quantum-RNG
===================

This racket package provides an implementation to fetch true random numbers
from the quantum random number generator of the Australian National University
(QRNG@ANU).

![LGPLv3](https://www.gnu.org/graphics/lgplv3-147x51.png)

More about QRNG@ANU at https://qrng.anu.edu.au/
