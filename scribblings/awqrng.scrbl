#lang scribble/manual

@require[@for-label[ANU-Web-Quantum-RNG
                    racket/base]
         scribble/examples]

@(define my-eval (make-base-eval))
@(my-eval '(require racket/base ANU-Web-Quantum-RNG))

@title{ANU-Web-Quantum-RNG}
@author+email["Tetsumi" "testumi@protonmail.com"]

@defmodule[ANU-Web-Quantum-RNG]

This package provides an implementation to fetch true random numbers from
the quantum random number generator of the Australian National University (QRNG@"@"ANU).

For a description of the QRNG@"@"ANU JSON API, go to @url{https://qrng.anu.edu.au/API/api-demo.php}.


@section{Basic Generation}

The following procedures are relying on a cache holding at most 1024 16-bits unsigned integers.
When the cache is empty, it's automatically refilled by fecthing 1024 numbers from QRNG@"@"ANU.

@defproc[(awqrng-u8) fixnum?]{
  Returns a random 8-bits unsigned number.
	
  @examples[
    (eval:alts (awqrng-u8) (eval:result @racketresult[51]))
    (eval:alts (awqrng-u8) (eval:result @racketresult[159]))
    (eval:alts (awqrng-u8) (eval:result @racketresult[77]))
  ]
}

@defproc[(awqrng-u16) fixnum?]{
  Returns a random 16-bits unsigned number.
	
  @examples[
    (eval:alts (awqrng-u16) (eval:result @racketresult[39299]))
    (eval:alts (awqrng-u16) (eval:result @racketresult[58914]))
    (eval:alts (awqrng-u16) (eval:result @racketresult[4026]))
  ]
}


@defproc[(awqrng-hex16) string?]{
  Returns a random 16-bits hexadecimal number as a string.

  @examples[
    (eval:alts (awqrng-hex16) (eval:result @racketresult["43cc"]))
    (eval:alts (awqrng-hex16) (eval:result @racketresult["8ac5"]))
    (eval:alts (awqrng-hex16) (eval:result @racketresult["612a"]))
  ]
}

@defproc[(awqrng-clear-cache) void?]{
  Clear the cache.	
}

@section{Fetching}

The following procedures bypass the cache to directly fetch numbers from QRNG@"@"ANU.

@defproc[(awqrng-fetch-u8 [length exact-nonnegative-integer?]) (listof fixnum?)]{
  Return a list of @racket[length] random 8-bits integers.
  
  @examples[
    (eval:alts 
      (awqrng-fetch-u8 10)
      (eval:result @racketresult['(86 49 232 168 205 223 7 61 27 38)]))
  ]
}

@defproc[(awqrng-fetch-u16 [length exact-nonnegative-integer?]) (listof fixnum?)]{
  Return a list of @racket[length] random 16-bits integers.
  
  @examples[
    (eval:alts 
      (awqrng-fetch-u16 10)
      (eval:result @racketresult[
        '(22577 48657 55574 39258 57038 2908 48371 25155 13422 51159)
      ]))
  ]
}

@defproc[(awqrng-fetch-hex16 [length exact-nonnegative-integer?]
                             [size exact-nonnegative-integer?])
         (listof string?)]{
  Return a list of @racket[length] strings, each string representing @racket[size] random
  16-bits hexadecimal numbers.

  @examples[
    (eval:alts 
      (awqrng-fetch-hex16 4 5)
      (eval:result @racketresult[
        '("f776f5cc9d" "f987d203ad" "8ad36a9517" "58744e09fa")
      ]))
    (eval:alts 
      (awqrng-fetch-hex16 5 10)
      '("782278ce43c8598c0ceb"
  	"733d4e5aafebbf690c1f"
  	"d10af473c98de0601e07"
  	"33548b255c1adb3d64a3"
  	"80f342093b4ab24dcada"))
  ]
}
